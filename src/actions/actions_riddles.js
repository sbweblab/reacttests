import axios from 'axios';

export const FETCH_RIDDLE = 'FETCH_RIDDLE';
export const FETCH_RIDDLES = 'FETCH_RIDDLES';
export const CREATE_RIDDLE = 'CREATE_RIDDLE'; 
export const DELETE_RIDDLE = 'DELETE_RIDDLE'; 
export const UPDATE_RIDDLE = 'DELETE_RIDDLE'; 

const ROOT_URL = "http://api.lodab.org/";
// const ROOT_URL = "http://api.sbweblab.com/";
//const API_KEY = '?key=lodab111';

export function fetchRiddles(page, count) {
    page = page < 1 ? 1 : page;
    count = count < 1 ? 10 : count;
    const request = axios.get(`${ROOT_URL}riddles?page=${page}&count=${count}`);
    return {
        type: FETCH_RIDDLES,
        payload: request
    };
}
export function fetchRiddle(id) {
    const request = axios.get(`${ROOT_URL}riddles/${id}`);
    return {
        type: FETCH_RIDDLE,
        payload: request
    };
}
export function createRiddle(values, callback) {
        const request = axios.post(`${ROOT_URL}riddles/`, values)
            .then (() => callback());
        return {
            type: CREATE_RIDDLE,
            payload: request
        };
}
export function updateRiddle(values, callback) {
    const request = axios.put(`${ROOT_URL}riddles/${values.Id}`, values)
        .then (() => callback());
    return {
        type: UPDATE_RIDDLE,
        payload: request
    };
}
