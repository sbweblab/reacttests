import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import promise from 'redux-promise';
import RiddlesIndex from './components/riddles_index';
import RiddleEdit from './components/riddle_edit';
import reducers from './reducers';
// import InitializeFromStateForm from './forms/riddle_form';
// import RiddleForm from './components/riddle_form';

import 'font-awesome/css/font-awesome.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import './index.css';

const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <div>
    <Provider store={createStoreWithMiddleware(reducers)}>
      <BrowserRouter>
        <div>
          <div style={{ clear: "both" }} />asdf
          <div className="content-wraper">
            <Switch>
              <Route path="/riddles/:id/edit" component={RiddleEdit} />
              <Route path="/riddles/:id" component={RiddlesIndex} />
              <Route path="/riddles" component={RiddlesIndex} />
              <Route path="/" component={RiddlesIndex} />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    </Provider>
  </div>
  , document.querySelector('#container'));
