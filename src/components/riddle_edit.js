import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchRiddle, updateRiddle } from '../actions/actions_riddles';
// import _ from 'lodash';
import { Link } from 'react-router-dom';
// import $ from 'jquery';


class RiddleEdit extends Component {
    state = {};
    componentDidMount() {
        this.props.fetchRiddle(this.props.match.params.id).then(data => {
            const { Id, Question, Answer, Hint, HonorableMention } = data.payload.data
            this.setState({
                Id: Id ? Id : 0,
                Question: Question ? Question : "",
                Answer: Answer ? Answer : "",
                Hint: Hint ? Hint : "",
                HonorableMention: HonorableMention ? HonorableMention : ""
            })
        }
        );
    }

    fields = [
        { name: "question", type: "textarea", value: "1" },
        { name: "answer", type: "textarea", value: "2" },
        { name: "hint", type: "textarea", value: "3" },
        { name: "honorablementions", type: "textarea", value: "4" }
    ];

    onSubmit(values) {
        debugger;
        this.props.updateRiddle(values, () => {
            this.props.history.push('/');
        });
    }

    renderFormField(field) {
        switch (field.type) {
            case "textarea":
                return (
                    <div className="form-item form-group has-danger">
                        <div className="form-item-label">{field.label}:</div>
                        <textarea name={field.name} 
                        value={field.value} 
                        className="form-textarea" 
                        placeholder={field.label} 
                        onChange={this.handleDataChange.bind(this)}/>
                    </div>
                );
            case "hidden":
                return (<input type="hidden" name={field.name} value={field.value}/>);
                default:
                return (<div>Error</div>);
        }
    }

    renderTextField(field) {
        const { meta: { touched, error } } = field;
        const className = `form-group ${touched && error ? "has-danger" : ''}`;
        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type="text"
                    {...field.input}
                />
                <div className="text-help">{touched ? error : ''}</div>
            </div>
        );

    }

    submitFormHandler(e) {
        e.preventDefault();
        console.log(this.state);
        this.props.updateRiddle(this.state, () => {
            this.props.history.push('/');
        });
        // debugger;
        // console.dir(this.refs.name.value); //will give us the name value
        // console.dir(this.refs); //will give us the name value
    }

    handleDataChange(e) {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        let { riddle } = this.props
        riddle = riddle ? riddle : { Id: 1, Question: 1, Hint: 1, HonorableMentions: 1, Answer: 11111 }
        if (!riddle.Id) return (<div>Loading...</div>);
        return (
            <div className="riddle-form">
                <form onSubmit={this.submitFormHandler.bind(this)}>
                    <div className="form-main-title">Edit Riddle</div>
                    {this.renderFormField({type:"hidden", name:"Id", value:this.state.Id})}
                    {this.renderFormField({type:"textarea", name:"Question", label:"Question", value:this.state.Question})}
                    {this.renderFormField({type:"textarea", name:"Answer", label:"Answer", value:this.state.Answer})}
                    {this.renderFormField({type:"textarea", name:"Hint", label:"Hint", value:this.state.Hint})}
                    {this.renderFormField({type:"textarea", name:"HonorableMention", label:"Honorable Mention", value:this.state.HonorableMention})}
                    <button type="submit" className="btn btn-primary">Submit</button>
                    <Link to="/" className="btn btn-danger">Cancel</Link>
                </form>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { riddle: state.riddles };
}

// eslint-disable-next-line 
function validate(values) {
    const errors = {};
    if (!values.title) {
        errors.title = "Enter a title!";
    }
    if (!values.categories) {
        errors.categories = "Enter a cat!";
    }
    if (!values.content) {
        errors.content = "Enter a content!";
    }
    return errors;
}

export default connect(mapStateToProps, { fetchRiddle, updateRiddle })(RiddleEdit);