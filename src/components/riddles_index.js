import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchRiddles } from '../actions/actions_riddles';
import _ from 'lodash';
// import { Link } from 'react-router-dom';
import $ from 'jquery';
import ReactPaginate from 'react-paginate';

const styles = {
riddleWrapper: { 
    border: "1px solid #17a2b8",
    margin: "5px",
    borderRadius: "5px",
    
    },
    riddleDate: {
        display:"block",
        fontWeight:"500"
    },
    riddleQuestion: {
        padding: "5px"
    },
    riddleHint: {
        display:"none"
    },
    riddleHonorable: {

    },
    riddleAnswer: {
        display:'none',
        padding:"10px",
    },
    riddleShowButton: {
    },
}


class RiddlesIndex extends Component {
    constructor(props) {
        debugger;
        super(props);
        debugger;
        this.state = {sidebarOpen:true, riddleId: 100}
        // this.setState({ sidebarOpen: false,
        //   riddleId: 0})
    }
      

    componentDidMount() {
        debugger;
        let page = 1;
        if (this.props.match.params.id) {page = this.props.match.params.id}
        this.getRiddlesFromApi(page, 5);
    }

    getRiddlesFromApi(page, count) {
        this.props.fetchRiddles(page, count);
    }

    handlePageClick = (data) => {
        let selected = data.selected + 1; //adding +1 as is returning 0 for Page 1 (offset)
        this.getRiddlesFromApi(selected, 5);
      };

    onClick() {
        var mainmenu = document.querySelector(`#answer${this.Id}`);
        $(mainmenu).slideToggle();
    }

    editRiddle = () => {
        debugger
        this.setState({riddleId: 1})
        this.setState({sidebarOpen: true,riddleId: this.Id        })
    }

    formatDate(date) {
        const d = new Date(date);
        const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        return `${month[d.getMonth()]} ${d.getDate()} ${d.getFullYear()}`
    }
    renderRiddles() {
        if (this === undefined) {
            return <div>Loading...</div>
        }
        return _.map(this.props.riddles.Results, riddle => {
            return (
                <li className="list-group-item" key={riddle.Id} style={styles.riddleWrapper}>
                    {/* <div className="riddleDate" style={styles.riddleDate}>{this.formatDate(riddle.date)}</div> */}
                    <div className="riddleQuestion" style={styles.riddleQuestion}>Question: {riddle.Question}</div>
                    {riddle.hint ? <div className="riddleHint" style={styles.riddleHint}>Hint: {riddle.Hint}</div> : ""}
                    <button className="btn btn-info" onClick={this.onClick.bind(riddle)}  style={styles.riddleShowButton}>
                        Show Answer
                    </button>
                    <div id={`answer${riddle.Id}`} style={styles.riddleAnswer} className="riddleAnswer">
                    Answer: {riddle.Answer}
                    {riddle.honorable ? <div className="riddleHonorable" style={styles.riddleHonorable}>Honorable Mention: {riddle.HonorableMentions}</div> : ""}

                    </div>
                    <button className="btn btn-info" onClick={this.editRiddle.bind(riddle)}  style={styles.riddleShowButton}>
                        Edit
                    </button>
                </li>
            );
        });
    }
    render() {
        let pageCount = this.props.riddles.PageCount ? this.props.riddles.PageCount : 0;
        return (
            <div>
                <div className="riddleTItle">Riddles</div> 
                <ReactPaginate 
                       breakLabel={"..."}
                       breakClassName={"break-me"}
                       containerClassName= {"page-navigation"}
                       pageClassName={"page-navigation-button"}
                       pageLinkClassName={"btn btn-default btn-lg"}
                       previousLinkClassName={"btn btn-default btn-lg"}
                       nextLinkClassName={"btn btn-default btn-lg"}
                       pageCount={pageCount}
                       marginPagesDisplayed={2}
                       pageRangeDisplayed={5}
                       onPageChange={this.handlePageClick}
                       subContainerClassName={"page-navigation-sub"}
                       activeClassName={"active"}
                       previousLabel={"Prev"} 
                       nextLabel={"Next"} />
                <ul className="list-group">
                    {this.renderRiddles()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    debugger;
    return { riddles: state.riddles, 
    sidebarOpen: state.sidebarOpen,
    riddleId: state.riddleId ? state.riddleId : 0};
}

export default connect(mapStateToProps, { fetchRiddles })(RiddlesIndex);