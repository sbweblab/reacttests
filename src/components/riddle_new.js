import React, { Component } from 'react';
import { Field as F, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createRiddle } from '../actions/actions_riddles';

class RiddlesNew extends Component {
    renderTextField(field) {
        const { meta: { touched, error } } = field;
        const className = `form-group ${ touched && error ? "has-danger" : ''}`;
        return (
            <div className={className}>
                <label>{field.label}</label>
                <input
                    className="form-control"
                    type="text"
                    {...field.input}
                />
                <div className="text-help">{touched ? error : ''}</div>
            </div>
        );

    }

    onSubmit(values) {
        debugger;
        this.props.createRiddle(values, () => {
            this.props.history.push('/');
        });
    }

    render() {
        const { handleSubmit } = this.props;
        return (
            <div>
                <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
                    <F name="title" component={this.renderTextField} label="Title for Post" />
                    {/* <F name="categories" component={this.renderTextField} label="Categories" /> */}
                    <F name="body" component={this.renderTextField} label="Post Content" />
                    <button type="submit" className="btn btn-primary">Submit</button> 
                    <Link to="/" className="btn btn-danger">Cancel</Link>
                </form>
            </div>
        );
    }
}

function validate(values) {
    const errors = {};
    if (!values.title) {
        errors.title = "Enter a title!";
    }
    if (!values.categories) {
        errors.categories = "Enter a cat!";
    }
    if (!values.content) {
        errors.content = "Enter a content!";
    }
    return errors;
}

export default reduxForm({
    validate,
    form: "PostsNewForm",
    initialValues: {
      blog_id: '1'
    }
})(
    connect(null, { createRiddle }) (RiddlesNew)
);