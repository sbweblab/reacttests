import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchRiddle } from '../actions/actions_riddles';
import _ from 'lodash';
import { Link } from 'react-router-dom';
import $ from 'jquery';

const styles = {
riddleWrapper: { 
    border: "1px solid #17a2b8",
    margin: "5px",
    borderRadius: "5px",
    
    },
    riddleDate: {
        display:"block",
        fontWeight:"500"
    },
    riddleQuestion: {
        padding: "5px"
    },
    riddleHint: {
        display:"none"
    },
    riddleHonorable: {

    },
    riddleAnswer: {
        display:'none',
        padding:"10px",
    },
    riddleShowButton: {
    },
}


class Riddle extends Component {
    componentDidMount() {
        this.props.fetchRiddle(this.props.match.params.id);
    }
    onClick() {
        var mainmenu = document.querySelector(`#answer${this.Id}`);
        $(mainmenu).slideToggle();
    }
    formatDate(date) {
        const d = new Date(date);
        const month = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        return `${month[d.getMonth()]} ${d.getDate()} ${d.getFullYear()}`
    }
    renderRiddle() {
        debugger;
        if (this === undefined || this.props.riddle === null) {
            return <div>Loading...</div>
        }
        let riddle = this.props.riddle;
            return (
                <li className="list-group-item" key={riddle.Id} style={styles.riddleWrapper}>
                    {/* <div className="riddleDate" style={styles.riddleDate}>{this.formatDate(riddle.date)}</div> */}
                    <div className="riddleQuestion" style={styles.riddleQuestion}>{riddle.Question}</div>
                    <div className="riddleHint" style={styles.riddleHint}>{riddle.Hint}</div>
                    <button className="btn btn-outline-info" onClick={this.onClick.bind(riddle)}  style={styles.riddleShowButton}>
                        Show Answer
                    </button>
                    <div id={`answer${riddle.Id}`} style={styles.riddleAnswer} className="riddleAnswer">
                    Answer: {riddle.Answer}
                    {riddle.honorable ? <div className="riddleHonorable" style={styles.riddleHonorable}>Honorable Mention: {riddle.HonorableMentions}</div> : ""}
                    </div>
                </li>
            );
    }
    render() {
        return (
            <div>
                {/* <EditDrawer content="asdfasfd" /> */}
                <div className="riddleTItle">Riddle History</div>
                <ul className="list-group">
                    {this.renderRiddle()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return { riddle: state.riddles };
}

export default connect(mapStateToProps, { fetchRiddle })(Riddle);