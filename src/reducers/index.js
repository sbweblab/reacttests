import { combineReducers  as cR } from 'redux';
import RiddlesReducer from './reducer_riddles';
// import { reducer as reduxFormReducer } from 'redux-form';

const rootReducer = cR({
  riddles: RiddlesReducer
});

export default rootReducer;
