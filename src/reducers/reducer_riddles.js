import { FETCH_RIDDLE, FETCH_RIDDLES } from '../actions/actions_riddles'
// import _ from 'lodash';

export default function (state = {}, action) {
    switch (action.type) {
        case FETCH_RIDDLE:
            return action.payload.data;
        case FETCH_RIDDLES:
            // debugger;
            return action.payload.data;
        // return _.mapKeys(action.payload.data.Results, "Id");
        default:
            return state;
    }
}